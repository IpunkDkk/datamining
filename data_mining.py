import pandas as pd 
import numpy as np
# import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split


data = pd.read_csv(r'Breast_cancer.csv')

# 1. jumlah data
# print(data.count())

# 2. sebutkan jumlah fitur
# data = data.loc[:, ~data.columns.str.contains('^Unnamed')]
# print(data.shape)

# 3. menghapus data id
# data = data.drop('id' , axis=1);
data = data.drop(['id','Unnamed: 32'] , axis=1);
# data = data.loc[:, ~data.columns.str.contains('^Unnamed')]
print(data.head)
for x in range(len(data['diagnosis'])):
	if ((data['diagnosis'][x]) == 'B'):
		data = data.replace(data['diagnosis'][x] , 1)
	elif ((data['diagnosis'][x]) == 'M'):
		data = data.replace(data['diagnosis'][x] , 2)
	# print(data['diagnosis'][x])
# print(data)

x = data.drop(columns='diagnosis')
y = data['diagnosis']

print (x)

# 5 dan 6 memisahkan data 70% training 30%testing
x_train,x_test,y_train,y_test = train_test_split(x,y,test_size=0.3)
# print(x_train.head())
# print(x_test.head())
# print(y_train)
# print(y_test)

